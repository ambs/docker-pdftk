FROM debian:buster-slim

WORKDIR /workdir
RUN mkdir -p /usr/share/man/man1/
RUN apt -y update && apt -y upgrade && apt -y install pdftk
